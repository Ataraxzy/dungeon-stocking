#!/usr/bin/env python
""" Build Script for DungeonStocking!


"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.1.0"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

import sys
from cx_Freeze import *

base = None
if sys.platform == "win32":
    base = "Console"



includefiles = []
includes = []
excludes = []
packages = ['CSV',
            'Random',
            'TextWrap']

setup(
    name = 'DungeonStocking',
    version = '1.1',
    description = "A Python implementation of Courtney Campbell's Interesting Treasure, and more.",
    author = 'Roger E. Burgess III',
    author_email = 'roger.burgess.iii@gmail.com',
    options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles, 'include_msvcr':True}},
    executables = [Executable('DungeonStocking!.py', base=base)]
)