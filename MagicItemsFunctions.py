#!/usr/bin/env python
""" [Insert a one-line description here.]

[If have more information to impart, do so here]
"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules
import random
import textwrap
import config
import WeaponFunctions as WF
import ArmorFunctions as AF


__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

def choose_source():
    """ Chooses the source for the magic items.
    Assigns the folder name of the source to 'sG_items_source'.


    :rtype : str
    """
    
    l_sources = config.csv_to_list('data/magicitems/item_source.csv')
    i_source = random.randint(1, 400)

    for line in l_sources:
        if i_source <= int(line['DieRoll']):
            s_items_source = (u'data/magicitems/' + line['Source'] + '/')
            break


    return s_items_source


def magic_item_generator(f_adj_value):
    """
    Note: Not ussing f_adj_value at the moment.
    We will be using it once the 1e tables get pushed out.

    """
    s_item_desc = u'{}\t[MAGIC ITEM]: '.format(config.s_dl_brace_dash)
    s_source = choose_source()
    s_item_list = s_source + 'magic_item_type.csv'
    l_item_type = config.csv_to_list(s_item_list)
    i_item_type = random.randint(1, 100)

    for line in l_item_type:
        if i_item_type <= int(line['DieRoll']):
            s_item = line['Type']
            break

            
    s_item_desc += u'{}'.format(s_item)
    
    if s_item == 'Potions':
        s_item_desc += choose_potions(s_source)
    elif s_item == 'Rings':
        s_item_desc += choose_rings(s_source)
    elif s_item == 'Scrolls':
        s_item_desc += choose_scrolls(s_source)
    elif s_item == 'Rods, Staffs, and Wands':
        s_item_desc += choose_wands(s_source)
    elif s_item == 'Miscellaneous Magic':
        s_item_desc += choose_misc_items(s_source)
    elif s_item == 'Swords':
        s_item_desc += choose_swords(s_source)
    elif s_item == 'Miscellaneous Weapon':
        s_item_desc += choose_misc_weapons(s_source)
    elif s_item == 'Armor':
        s_item_desc += choose_armor(s_source)
    else: 
        s_item_desc += "We shouldn't be here!"

    return s_item_desc


def choose_potions(s_items_source='data/magicitems/ACKS/'):
    """
    
    """

    s_item_list = s_items_source + 'potions.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    
    s_color = AF.armor_weirdness_color()
    s_items = (u'\n\t\t[POTION]: {}'.format(s_item_type))
    s_items += (u'{0}{1}'.format(s_color,
                                config.s_dl_brace_dash))
    
    return s_items


def choose_rings(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'rings.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    

    s_items = (u'\n\t\t[RING]: {}'.format(s_item_type))
    # Choose a substance.
    i_sub_roll = random.randint(1, 100)
    if i_sub_roll < 80:
        s_items += (u'\n\t\t[Material]: ' + WF.spec_blade_mats())
    else:
        s_items += (u'' + AF.armor_weirdness_substance())

    s_items += config.s_dl_brace_dash
    return s_items


def choose_scrolls(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'scrolls.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    

    s_items = (u'\n\t\t[SCROLL]: {}{}'.format(s_item_type,
                                                 config.s_dl_brace_dash))

    
    return s_items


def choose_wands(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'rods_staves_wands.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    s_item_type += AF.generate_armor_wooden()

    s_items = (u'\n\t\t[ROD, STAFF or WAND]: {}{}'.format(s_item_type,
                                                config.s_dl_brace_dash))

    
    return s_items


def choose_misc_items(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'misc_magic.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    

    s_items = (u'\n\t\t[MISC ITEM]: {}{}'.format(s_item_type,
                                                 config.s_dl_brace_dash))

    
    return s_items


def choose_swords(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'swords.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            break

    i_rand_sword_type = random.randint(1, 100)

    if i_rand_sword_type <= 100:
        s_sword = 'Tech Blades Special'
    if i_rand_sword_type <= 97:
        s_sword = 'Tech Blades'

    if i_rand_sword_type <= 90:
        s_sword = 'Blades Special'
    if i_rand_sword_type <= 84:
        s_sword = 'Blades'

    if i_rand_sword_type <= 30:
        s_sword = 'Daggers Special'
    if i_rand_sword_type <= 27:
        s_sword = 'Daggers'

    s_items = (u'\n\t[Magical Enhancement]: {}'.format(s_item_type))

    s_items += WF.weapon_generation(s_weapon_type=s_sword)

    return s_items


def choose_misc_weapons(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    
    s_item_list = s_items_source + 'misc_weapons.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)
    
    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Type']
            s_action = line['Action']
            break



    if s_action == 'Roll ammo':
        s_misc = 'Ammo'

    if s_action == 'Roll hafted':
        s_misc = 'Hafted Special'

    if s_action == 'Roll missile':
        s_misc = 'Missile Special'

    if s_action == 'Roll polearms':
        s_misc = 'Polearms Special'

    if s_action == 'Roll misc':
        s_misc = 'Misc Special'

    if s_action == 'Roll tech':
        i_tech_roll = random.randint(1, 100)
        if i_tech_roll <= 100:
            s_misc = 'Guns'
        if i_tech_roll <= 60:
            s_misc = 'Lasers'
        if i_tech_roll <= 55:
            s_misc = 'Lasers Special'
        if i_tech_roll <= 45:
            s_misc = 'Lasers Flawed'
        if i_tech_roll <= 40:
            s_misc = 'Tech Missiles'
        if i_tech_roll <= 30:
            s_misc = 'Tech Missiles Special'
        if i_tech_roll <= 25:
            s_misc = 'Tech Missiles Flawed'
        if i_tech_roll <= 15:
            s_misc = 'Tech Hvy'
        if i_tech_roll <= 8:
            s_misc = 'Tech Hvy Special'
        if i_tech_roll <= 5:
            s_misc = 'Tech Hvy Flawed'

    s_items = (u'\n\t\t[MISC WEAPON]: {}'.format(s_item_type))

    s_items += WF.weapon_generation(s_weapon_type=s_misc)

    return s_items


def choose_armor(s_items_source='data/magicitems/ACKS/'):
    """
    
    """
    s_items = ''
    s_item_list = s_items_source + 'armor.csv'
    l_items = config.csv_to_list(s_item_list)
    i_rand_items = random.randint(1, 100)


    for line in l_items:
        if i_rand_items <= int(line['DieRoll']):
            s_item_type = line['Armor']
            s_action = line['Action']
            i_value = int(line['Value'])
            break

    s_items += (u': '+ s_item_type)

    if s_action == 'Roll armor and shield':
        l_armors = config.csv_to_list('data/armor/armor_type.csv')
        s_items += AF.armor_suit(l_armor=l_armors,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=(i_value/2))

        l_shields = config.csv_to_list('data/armor/armor_shield_type.csv')
        s_items += AF.armor_suit(l_armor=l_shields,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=(i_value/2))

    if s_action == 'Roll bad armor and shield':
        l_armors = config.csv_to_list('data/armor/armor_type.csv')
        s_items += AF.armor_suit(l_armor=l_armors,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=random.randint(2,150))

        l_shields = config.csv_to_list('data/armor/armor_shield_type.csv')
        s_items += AF.armor_suit(l_armor=l_shields,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=i_value)

    if s_action == 'Roll armor':
        l_armors = config.csv_to_list('data/armor/armor_type.csv')
        s_items += AF.armor_suit(l_armor=l_armors,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=i_value)

    if s_action == 'Roll shield':
        l_shields = config.csv_to_list('data/armor/armor_shield_type.csv')
        s_items += AF.armor_suit(l_armor=l_shields,
                                 f_split=0,
                                 f_goods_value_low=0,
                                 f_goods_value_high=0,
                                 f_as_rand_base_price=i_value)

    s_items += config.s_dl_brace_dash

    
    return s_items