#! /usr/bin/env python
''' GemFunctions serves to populate 'gem' rolls with gems of various sizes and value.


'''
# TODO: Add descriptions to gem types.
# import statments
import csv
import random
import textwrap

__author__ = 'Roger E. Burgess, III'
__copyright__ = "Copyright 2014, Red Flag & Roger E. Burgess, III"
__credits__ = ["Courtney Cambell", "David Arneson", "Gary Gygax"]
__license__ = "GPL v3 or later"
__version__ = "0.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"


def gem_main(f_split_value): #f_split_value
    s_gems =''
    f_next_gem_value = 0.0
    f_gem_split = f_split_value

    if f_gem_split <= 10.0:
        s_gems_temp = (u'[STONES]: The village fool might buy them from '
                       u'you for {:,.2f} sp, if only to throw them at you.\n'.format(
                       f_gem_split))

        s_gems = '\n' + textwrap.fill(s_gems_temp,
                            initial_indent='    ',
                            subsequent_indent='    ')

    while f_gem_split > 10:
        i_value_divisor = random.randint(1, 6)
        f_next_gem_value = f_gem_split / i_value_divisor
        f_gem_split -= f_next_gem_value

        # Categories and their counters
        (lm_ornamental,
        lm_semi,
        lm_fancy,
        lm_precious,
        lm_gemstone,
        lm_jewel,
        lm_cut,
        lm_overflow,
        im_ornamental_ctr,
        im_semi_ctr,
        im_fancy_ctr,
        im_precious_ctr,
        im_gemstone_ctr,
        im_jewel_ctr,
        im_cut_ctr,
        im_overflow_ctr) = populate_gem_categories()

        (lm_size,
        im_size_ctr) = populate_gem_size()

        (lm_quality,
        im_quality_ctr) = populate_gem_quality()

        (lm_val_category,
        im_val_ctr) = populate_gem_value_categories()

        ornamental_limit = 25.0
        semi_limit = 75.0
        fancy_limit = 250.0
        precious_limit = 750.0
        gem_limit = 2500.0
        # jewel_limit = 10000.0

        # Find the initial value category of the treasure.
        # We're going to add the modifiers to it to get a new category.
        for line in lm_val_category:
            i_value_category = 0
            if f_next_gem_value <= float(line['Value']):
                i_value_category = int(line['Number'])
                break

        # Determine the size and quality
        i_gem_size_roll = random.randint(1, 400)
        i_gem_quality_roll = random.randint(1, 400)

        # Size of gem?
        s_gem_size = u'\n\t\t[SIZE]: '
        for line in lm_size:
            if i_gem_size_roll <= int(line['DieRoll']):
                s_gem_size += line['Size']
                i_size_mod = int(line['Modifier'])
                break

        # Quality of gem?
        s_gem_quality = u'\n\t\t[QUALITY]: '
        for line in lm_quality:
            if i_gem_quality_roll <= int(line['DieRoll']):
                s_gem_quality += line['Quality']
                i_quality_mod = int(line['Modifier'])
                break

        # Cut of gem?
        s_gem_cut = u'\n\t[CUT]: '
        i_rand_cut = random.randint(0, im_cut_ctr)
        s_gem_cut += str(lm_cut[i_rand_cut]['Name'])
        s_gem_desc = u'[CUT DESCRIPTION]: ' + lm_cut[i_rand_cut]['Description']
        s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
        s_gem_cut_desc = u'\n\t' + s_gem_wrap

        i_full_modifier = i_value_category+ i_quality_mod + i_size_mod
        if i_full_modifier > im_val_ctr:
            i_full_modifier = im_val_ctr

        if i_full_modifier < 0: i_full_modifier = 0

        for line in lm_val_category:
            if i_full_modifier <= int(line['Number']):
                s_gem_value = u'\n\t\t[VALUE TO COLLECTOR]: {:,.2f} sp.\n'.format(
                float(line['Value']))
                # in case we ever need it:
                f_gem_value = float(line['Value'])
                break

        if f_next_gem_value <= ornamental_limit:
            i_rand_ornament = random.randint(0, im_ornamental_ctr)

            s_gems += u'\n\t[GEM TYPE]: {}. An ornamental stone.'.format(
                lm_ornamental[i_rand_ornament]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_ornamental[i_rand_ornament]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap

        elif f_next_gem_value <= semi_limit:
            i_rand_semi = random.randint(0, im_semi_ctr)

            s_gems += u'\n\t[GEM TYPE] {}.  A semi-precious stone.'.format(
                lm_semi[i_rand_semi]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_semi[i_rand_semi]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap

        elif f_next_gem_value <=fancy_limit:
            i_rand_fancy = random.randint(0, im_fancy_ctr)

            s_gems += u'\n\t[GEM TYPE] {}.  A fancy stone.'.format(
                lm_fancy[i_rand_fancy]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_fancy[i_rand_fancy]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap

        elif f_next_gem_value <= precious_limit:
            i_rand_precious = random.randint(0, im_precious_ctr)

            s_gems += u'\n\t[GEM TYPE] {}.  A precious stone.'.format(
                lm_precious[i_rand_precious]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_precious[i_rand_precious]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap

        elif f_next_gem_value <= gem_limit:
            i_rand_gem = random.randint(0, im_gemstone_ctr)

            s_gems += u'\n\t[GEM TYPE] {}.  A gemstone.'.format(
                lm_gemstone[i_rand_gem]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_gemstone[i_rand_gem]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap

        else: # f_split_value > 2500.0
            i_rand_jewel = random.randint(0, im_jewel_ctr)

            s_gems += u'\n\t[GEM TYPE] {}.  A jewel of great price.'.format(
                lm_jewel[i_rand_jewel]['Name'])
            s_gem_desc = '[DESCRIPTION]: ' + lm_jewel[i_rand_jewel]['Description']
            s_gem_wrap = textwrap.fill(s_gem_desc, initial_indent=
                                '', subsequent_indent=
                                '        ')
            s_gems += u'\n\t' + s_gem_wrap


        s_quickly_appriases_to = u'\n\t\t[QUICK APPRAISAL VALUE]: {:,.2f}'.format(
            f_next_gem_value)

        s_gems += u'{}{}{}{}{}{}'.format(s_quickly_appriases_to,
                                         s_gem_cut,
                                         s_gem_cut_desc,
                                         s_gem_size,
                                         s_gem_quality,
                                         s_gem_value)

    return s_gems


def populate_gem_categories():

    l_ornamental = []
    l_semi = []
    l_fancy = []
    l_precious = []
    l_gemstone = []
    l_jewel = []
    l_cut = []
    l_overflow = []

    i_ornamental_ctr = -1
    i_semi_ctr = -1
    i_fancy_ctr = -1
    i_precious_ctr = -1
    i_gemstone_ctr = -1
    i_jewel_ctr = -1
    i_cut_ctr = -1
    i_overflow_ctr = -1

    with open('data/gems/gem_category.csv', newline='') as gem_f:
            d_gem = csv.DictReader(gem_f, delimiter=',', skipinitialspace=True)
            for line in d_gem:
                if line['Category'] == 'Ornamental':
                    i_ornamental_ctr +=1
                    l_ornamental.append(line)
                elif line['Category'] == 'Semi-precious':
                    i_semi_ctr +=1
                    l_semi.append(line)
                elif line['Category'] == 'Fancy':
                    i_fancy_ctr +=1
                    l_fancy.append(line)
                elif line['Category'] == 'Precious':
                    i_precious_ctr +=1
                    l_precious.append(line)
                elif line['Category'] == 'Gem':
                    i_gemstone_ctr +=1
                    l_gemstone.append(line)
                elif line['Category'] == 'Jewel':
                    i_jewel_ctr +=1
                    l_jewel.append(line)
                elif line['Category'] == 'Cut':
                    i_cut_ctr +=1
                    l_cut.append(line)
                else:
                    i_overflow_ctr +=1
                    l_overflow.append(line)

    return (l_ornamental,
            l_semi,
            l_fancy,
            l_precious,
            l_gemstone,
            l_jewel,
            l_cut,
            l_overflow,
            i_ornamental_ctr,
            i_semi_ctr,
            i_fancy_ctr,
            i_precious_ctr,
            i_gemstone_ctr,
            i_jewel_ctr,
            i_cut_ctr,
            i_overflow_ctr)


def populate_gem_quality():

    l_quality = []
    i_quality_ctr = -1

    with open('data/gems/gem_quality.csv', newline='') as gem_f:
            d_gem = csv.DictReader(gem_f, delimiter=',', skipinitialspace=True)
            for line in d_gem:
                i_quality_ctr +=1
                l_quality.append(line)

    return l_quality, i_quality_ctr


def populate_gem_size():

    l_size = []
    i_size_ctr = -1

    with open('data/gems/gem_size.csv', newline='') as gem_f:
            d_gem = csv.DictReader(gem_f, delimiter=',', skipinitialspace=True)
            for line in d_gem:
                i_size_ctr +=1
                l_size.append(line)

    return l_size, i_size_ctr


def populate_gem_value_categories():

    l_value_categories = []
    i_value_categories_ctr = -1

    with open('data/gems/gem_value_categories.csv', newline='') as gem_f:
            d_gem = csv.DictReader(gem_f, delimiter=',', skipinitialspace=True)
            for line in d_gem:
                i_value_categories_ctr +=1
                l_value_categories.append(line)

    return l_value_categories, i_value_categories_ctr