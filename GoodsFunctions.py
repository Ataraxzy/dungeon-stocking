#!/usr/bin/env python
""" Functions for determining what goods are found as treasure.

goods_main() does the bulk of the work.
goods_low_value() pulls the list of low-value goods.
goods_high_value() pulls the list of high-value goods.

goods_main() picks high-value goods 80% of the time, because
30,000 sp in coffee beans is cruel.
"""
# TODO: use JewelFunc.populate_utensils()
# TODO: Cloth or Fabric, select type.
# TODO: High_value: Leather, Fabric, pull from tables.
# TODO: Metal bar stock: select metal type based on value.

# import RoomStockingFunctions as RoomStockFunc
import csv
import random
import textwrap
import config
import ArmorFunctions as AF
import WeaponFunctions as WF
from JewelFunctions import populate_utensils

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"


def goods_main(f_split_value):
    """

    :param f_split_value: float
    :return: s_goods: str
    """
    # If the incoming value is unusually high or low, use low_ or high_ as
    # appropriate.
    if f_split_value <= 50.0:
        l_goods, i_goods_ctr = low_value_goods()

    elif f_split_value >= 5000.0:
        l_goods, i_goods_ctr = high_value_goods()

    # Neither high nor low, randomly pick:
    # Heavily weighted towards high, low value goods can be cruel.
    else:

        i_high_or_low = random.randint(1, 20)

        if i_high_or_low >= 16:
            l_goods, i_goods_ctr = low_value_goods()
        else:
            l_goods, i_goods_ctr = high_value_goods()

    i_goods_roll = random.randint(1, i_goods_ctr)
    s_goods = ''

    goods_line = l_goods[i_goods_roll]

    s_goods_type = goods_line['Goods']
    s_unit = goods_line['Unit']
    s_description = '[DESCRIPTION]: '
    s_description += goods_line['Description']

    wrap_desc = textwrap.fill(s_description, initial_indent='        ',
                              subsequent_indent='        ')

    s_goods += (u'\n\t[GOOD]: {}.\n\t\t[UNITS]: {}.\n{}'.format
                (
                    s_goods_type, s_unit, wrap_desc
                )
    )




    # TODO: Pricing values from the sub-tables, not the main table.
    # TODO: Use main table pricing as 'This stuff range(x, y)'


    if goods_line['Goods'] == 'Armor':
        s_goods += AF.armor_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Weapon':
        s_goods += WF.weapon_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Bar: Magical metal':
        s_goods += magic_bar_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Bar: Rare metal':
        s_goods += rare_bar_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Bar: Common metal':
        s_goods += common_bar_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Metal bar stock':
        s_goods += common_bar_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Scrolls & books':
        s_goods += book_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Narcotics or medicine':
        s_goods += narco_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Religious artifacts':
        s_goods += religious_art_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Lumber and Ivory':
        s_goods += wood_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Lumber: Normal':
        s_goods += wood_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Fabrics':
        s_goods += fabrics_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Laboratory items':
        s_goods += lab_item_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Ingot: Rare':
        s_goods += rare_ingot_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Ingot: Magical':
        s_goods += magic_ingot_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Ingot: Common':
        s_goods += common_ingot_generation(goods_line, f_split_value)

    elif goods_line['Goods'] == 'Pewter':
        s_goods += utensil_item_generation()

    elif goods_line['Goods'] == 'Brass':
        s_goods += utensil_item_generation()

    elif goods_line['Goods'] == 'Copperware':
        s_goods += utensil_item_generation()

    elif goods_line['Goods'] == 'Ceramics':
        s_goods += utensil_item_generation()

    elif goods_line['Goods'] == 'Wooden Items':
        s_goods += utensil_item_generation()

    elif goods_line['Goods'] == 'Furs, Skins and Hides':
        s_goods += skins_generation()

    elif goods_line['Goods'] == 'Dressed stone':
        s_goods += stone_generation()

    # Before we can fill these out, we need to create .csv files for them.

    #elif goods_line['Goods'] == 'Coffee or tea':
    #elif goods_line['Goods'] == 'Spices and herbs':
    #elif goods_line['Goods'] == 'Wine':
    #elif goods_line['Goods'] == 'Beer':
    #elif goods_line['Goods'] == 'Liquor':
    #elif goods_line['Goods'] == 'Wine: Rare':
    #elif goods_line['Goods'] == 'Beer: Rare':
    #elif goods_line['Goods'] == 'Liquor: Rare':


    else:
        s_goods += handle_the_rest(goods_line, f_split_value)
        




    return s_goods


def low_value_goods():
    """ Puts goods_low_value.csv into a list of dicts.
    :rtype : list, int # TODO: str
    """
    with open('data/goods/goods_low_value.csv', newline='') as glv_f:
        dr_glv = csv.DictReader(glv_f, delimiter=',', skipinitialspace=True)

        lf_low_goods = []
        if_low_goods_ctr = -1

        for line in dr_glv:
            if_low_goods_ctr += 1
            lf_low_goods.append(line)

        return lf_low_goods, if_low_goods_ctr


def high_value_goods():
    """ Puts goods_high_value.csv into a list of dicts.
     :rtype : list, str
     """
    with open('data/goods/goods_high_value.csv', newline='') as ghv_f:
        dr_ghv = csv.DictReader(ghv_f, delimiter=',', skipinitialspace=True)

        lf_high_goods = []
        if_high_goods_ctr = -1

        for line in dr_ghv:
            if_high_goods_ctr += 1
            lf_high_goods.append(line)

        return lf_high_goods, if_high_goods_ctr


def magic_ingot_generation(d_line, f_split):


    # TODO: Value Ingots in goods_metals.csv
    mig_s_goods = ''
    with open('data/goods/goods_metals.csv',
              newline='') as gm_f:
        dr_gm = csv.DictReader(gm_f, delimiter=',',
                               skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        if f_goods_value_low > f_split:

            lf_metal_rare = []
            if_metal_rare_ctr = -1

            for line_gm in dr_gm:
                if line_gm['Classification'] == 'Common':
                    if_metal_rare_ctr += 1
                    lf_metal_rare.append(line_gm)

            i_metal_rare_roll = random.randint(0,
                                               if_metal_rare_ctr)

            s_metal_rare_type = lf_metal_rare[i_metal_rare_roll][
                'Metal']
            mig_s_goods += u'\n'
            s_goods_temp = (
                u'[VALUE]: Somebody thought they were '
                u'being clever, painting mere {} to look '
                u'like something more valuable.  It might be '
                u'worth {:,.2f} sp. if you could find a buyer.'
                u'\n').format(s_metal_rare_type, f_split)

            mig_s_goods += textwrap.fill(s_goods_temp, initial_indent=
            '        ',
                                     subsequent_indent=
                                     '        ') + '\n'
        else:
            mig_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                        u'{:,.2f}.\n').format(s_price)

            lf_metal_rare = []
            if_metal_rare_ctr = -1

            for line_gm_magic in dr_gm:
                if line_gm_magic['Classification'] == 'Magical':
                    if_metal_rare_ctr += 1
                    lf_metal_rare.append(line_gm_magic)

            i_metal_rare_roll = random.randint(0, if_metal_rare_ctr)

            s_metal_type = lf_metal_rare[i_metal_rare_roll][
                'Metal']
            s_metal_desc = u'[DESCRIPTION]: '
            s_metal_desc += lf_metal_rare[i_metal_rare_roll][
                'Description']

            wrap_metal_desc = textwrap.fill(s_metal_desc,
                                            initial_indent=
                                            '        ',
                                            subsequent_indent=
                                            '        ')

            mig_s_goods += u'\n\t\t[METAL TYPE]: {}\n{}' \
                       u'\n'.format(s_metal_type, wrap_metal_desc)

    return mig_s_goods


def rare_ingot_generation(d_line, f_split):


    rig_s_goods = ''
    with open('data/goods/goods_metals.csv', newline='') as gr_f:
        dr_gr_comm = csv.DictReader(gr_f, delimiter=',',
                                    skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        # Case: incoming value < than lowest item price per unit
        # We then choose a common metal.
        if f_goods_value_low > f_split:

            lf_metal_rare_comm = []
            if_metal_rare_comm_ctr = -1

            for line_gr_comm in dr_gr_comm:
                if line_gr_comm['Classification'] == 'Common':
                    if_metal_rare_comm_ctr += 1
                    lf_metal_rare_comm.append(line_gr_comm)

            i_metal_rare_roll = random.randint(1,
                                               if_metal_rare_comm_ctr)

            s_metal_rare_type = \
                lf_metal_rare_comm[i_metal_rare_roll]['Metal']

            s_goods_temp = (
                u'[VALUE]: Somebody thought they were '
                u'being clever, painting mere {} to look '
                u'like something more valuable.  It might be '
                u'worth {:,.2f} sp. if you could find a buyer.'
                u'\n').format(s_metal_rare_type, f_split)

            rig_s_goods += '\n' + textwrap.fill(s_goods_temp, initial_indent=
            '        ',
                                     subsequent_indent=
                                     '        ') + '\n'
        else:
            with open('data/goods/goods_metals.csv',
                      newline='') as gr_f:
                dr_gr = csv.DictReader(gr_f, delimiter=',',
                                       skipinitialspace=True)
                rig_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                            u'{:,.2f}.\n').format(s_price)
                lf_metal_rare = []
                if_metal_rare_ctr = -1

                for line_gr in dr_gr:
                    if line_gr['Classification'] == 'Rare':
                        if_metal_rare_ctr += 1
                        lf_metal_rare.append(line_gr)

                i_metal_rare_roll = random.randint(1,
                                                   if_metal_rare_ctr)

                s_metal_type = lf_metal_rare[i_metal_rare_roll][
                    'Metal']
                s_metal_desc = u'[DESCRIPTION]: '
                s_metal_desc += lf_metal_rare[i_metal_rare_roll][
                    'Description']

                wrap_metal_desc = textwrap.fill(s_metal_desc,
                                                initial_indent=
                                                '        ',
                                                subsequent_indent=
                                                '        ')

                rig_s_goods += u'\n\t\t[METAL TYPE]: {}\n{}\n'.format(
                    s_metal_type, wrap_metal_desc)

    return rig_s_goods


def magic_bar_generation(d_line, f_split):
    mbg_s_goods = ''
    with open('data/goods/goods_metals.csv',
              newline='') as gbm_f:
        dr_gbm = csv.DictReader(gbm_f, delimiter=',',
                                skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        # Case: incoming value lower than lowest item price per unit
        # We then choose a common metal.
        if f_goods_value_low > f_split:
            s_funky = random.choice(['Glow', 'Sparkle', 'Color'
                                     ' shift', 'Smell like fresh'
                                     ' bread', 'Hum in the '
                                     'presence of stress'])

            lf_metal_magic = []
            if_metal_magic_ctr = -1

            for line_gbm in dr_gbm:
                if line_gbm['Classification'] == 'Common':
                    if_metal_magic_ctr += 1
                    lf_metal_magic.append(line_gbm)

            i_metal_magic_roll = random.randint(0,
                                                if_metal_magic_ctr)

            s_metal_magic_type = \
                lf_metal_magic[i_metal_magic_roll]['Metal']

            s_goods_temp = (u'[VALUE]: The strange emanations '
                            u'of this place have caused '
                            u'this otherwise normal {} '
                            u'to {}.  It might be worth '
                            u'{:,.2f} sp. if you could '
                            u'find a buyer.\n').format \
                (
                    s_metal_magic_type, s_funky,
                    f_split)

            mbg_s_goods += textwrap.fill(s_goods_temp, initial_indent=
            '        ', subsequent_indent=
                                     '        ') + '\n'
        else:
            with open('data/goods/goods_metals.csv',
                      newline='') as gbm_comm_f:
                dr_gbm_comm = csv.DictReader(gbm_comm_f,
                                             delimiter=',',
                                             skipinitialspace=True)

                mbg_s_goods += u'\n\t\t[VALUE PER UNIT]: ' \
                           u'{:,.2f}.\n'.format(s_price)
                lf_metal_rare = []
                if_metal_rare_ctr = -1

                for line_gbm_comm in dr_gbm_comm:
                    if line_gbm_comm['Classification'] == 'Magical':
                        if_metal_rare_ctr += 1
                        lf_metal_rare.append(line_gbm_comm)

                i_metal_rare_roll = random.randint \
                    (0, if_metal_rare_ctr)

                s_metal_type = \
                    lf_metal_rare[i_metal_rare_roll]['Metal']
                s_metal_desc = u'[DESCRIPTION]: '
                s_metal_desc += \
                    lf_metal_rare[i_metal_rare_roll][
                        'Description']

                wrap_metal_desc = textwrap.fill(s_metal_desc,
                                                initial_indent=
                                                '        ',
                                                subsequent_indent=
                                                '        ')

                mbg_s_goods += u'\n\t\t[METAL TYPE]: {}\n{}' \
                               u'\n'.format(s_metal_type,
                                            wrap_metal_desc)

    return mbg_s_goods


def rare_bar_generation(d_line, f_split):

    rbg_s_goods = ''
    with open('data/goods/goods_metals.csv',
              newline='') as gbr_f:
        dbr_gr = csv.DictReader(gbr_f, delimiter=',',
                                skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        # Case: incoming value < than lowest item price per unit
        # We then choose a common metal.
        if f_goods_value_low > f_split:

            lf_metal_rare_comm = []
            if_metal_rare_comm_ctr = -1

            for line_gbr_comm in dbr_gr:
                if line_gbr_comm['Classification'] == 'Common':
                    if_metal_rare_comm_ctr += 1
                    lf_metal_rare_comm.append(line_gbr_comm)

            i_metal_rare_roll = random.randint(0,
                                               if_metal_rare_comm_ctr)

            s_metal_rare_type = \
                lf_metal_rare_comm[i_metal_rare_roll]['Metal']

            s_goods_temp = (
                u'\n[VALUE]: Somebody thought they were '
                u'being clever, painting mere {} to look '
                u'like something more valuable.  It might be '
                u'worth {:,.2f} sp. if you could find a buyer.'
                u'\n').format(s_metal_rare_type, f_split)

            rbg_s_goods += '\n'+ textwrap.fill(s_goods_temp, initial_indent=
            '        ',
                                     subsequent_indent=
                                     '        ')
        else:
            with open('data/goods/goods_metals.csv',
                      newline='') as gbr_f:
                dr_gbr = csv.DictReader(gbr_f, delimiter=',',
                                        skipinitialspace=True)

                rbg_s_goods += u'\n\t\t[VALUE PER UNIT]: ' \
                           u'{:,.2f}.\n'.format(s_price)
                lf_metal_rare = []

                if_metal_rare_ctr = -1

                for line_gbr in dr_gbr:
                    if line_gbr['Classification'] == 'Rare':
                        if_metal_rare_ctr += 1
                        lf_metal_rare.append(line_gbr)
                        y = 3
                i_metal_rare_roll = random.randint(0,
                                                   if_metal_rare_ctr)

                s_metal_type = lf_metal_rare[i_metal_rare_roll][
                    'Metal']
                s_metal_desc = u'[DESCRIPTION]: '
                s_metal_desc += lf_metal_rare[i_metal_rare_roll][
                    'Description']

                wrap_metal_desc = textwrap.fill(s_metal_desc,
                                                initial_indent=
                                                '        ',
                                                subsequent_indent=
                                                '        ')

                rbg_s_goods += u'\n\t\t[METAL TYPE]: {}\n{}' \
                           u'\n'.format(s_metal_type,
                                        wrap_metal_desc)

    return rbg_s_goods


def common_ingot_generation(d_line, f_split):
    cig_s_goods = ''
    with open('data/goods/goods_metals.csv',
              newline='') as gm_f:
        dr_gm = csv.DictReader(gm_f, delimiter=',',
                               skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        cig_s_goods += (u'\n\t\t[VALUE PER UNIT]: {:,.2f}.'
                    u'\n').format(s_price)

        lf_metal_comm = []
        if_metal_comm_ctr = -1

        for line_gm_comm in dr_gm:
            if line_gm_comm['Classification'] == 'Common':
                if_metal_comm_ctr += 1
                lf_metal_comm.append(line_gm_comm)

        i_metal_rare_roll = random.randint(0, if_metal_comm_ctr)

        s_metal_type = lf_metal_comm[i_metal_rare_roll]['Metal']
        s_metal_desc = u'[DESCRIPTION]: '
        s_metal_desc += lf_metal_comm[i_metal_rare_roll][
            'Description']

        wrap_metal_desc = textwrap.fill(s_metal_desc,
                                        initial_indent=
                                        '        ',
                                        subsequent_indent=
                                        '        ')

        cig_s_goods += u'\n\t\t[METAL TYPE]: {}\n{}\n'.format(
            s_metal_type, wrap_metal_desc)

    return cig_s_goods


def common_bar_generation(d_line, f_split):
    cbg_s_goods = ''
    with open('data/goods/goods_metals.csv',
              newline='') as gr_f:
        dbr_gr = csv.DictReader(gr_f, delimiter=',',
                                skipinitialspace=True)
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        lf_metal_rare = []
        if_metal_rare_ctr = -1

        for line_gbr in dbr_gr:
            if line_gbr['Classification'] == 'Common':
                if_metal_rare_ctr += 1
                lf_metal_rare.append(line_gbr)

        i_metal_rare_roll = random.randint(0, if_metal_rare_ctr)

        s_metal_rare_type = lf_metal_rare[i_metal_rare_roll][
            'Metal']

        s_goods_temp = (
            u'\n[VALUE]: Somebody thought they were being '
            u'clever, Painting mere {} to look like '
            u'something more valuable.  It might be worth'
            u'{:,.2f} sp. if you could find a buyer.'
            u'\n').format(s_metal_rare_type, f_split)

        cbg_s_goods += textwrap.fill(s_goods_temp, initial_indent=
        '        ',
                                     subsequent_indent='        ')

        cbg_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                        u'{:,.2f}.\n').format(s_price)

    return cbg_s_goods


def book_generation(d_line, f_split):

    bg_s_goods = ''
    with open('data/goods/goods_books.csv',
              newline='') as gb_f:
        dr_gb = csv.DictReader(gb_f, delimiter=',',
                               skipinitialspace=True)

        # Not used at the moment.*-
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])

        lf_books = []
        if_books_ctr = -1

        for line_gb in dr_gb:
            if_books_ctr += 1
            lf_books.append(line_gb)


        # Guard against stupid low values:
        f_book_split = f_split
        if f_book_split <= 200.0:
            bg_s_goods += u'\n\t\t[OBJECTS]: {:,.2f} sp in ' \
                       u'writing materials.'.format(f_book_split)
            f_book_split = -1

        f_next_book_value = 0.0
        while f_book_split > 200.0:
            # Find the value of the book:
            i_value_divisor = random.randint(1, 6)
            f_next_book_value = f_book_split / i_value_divisor
            f_book_split -= f_next_book_value

            # Roll for the contents:
            i_books_roll = random.randint(0, if_books_ctr)

            s_topic = lf_books[i_books_roll]['Topic']
            i_die_size = int(lf_books[i_books_roll]['Die Size'])
            s_library_TF = lf_books[i_books_roll]['Library']

            # How many answers the book gives:
            i_die_roll = random.randint(1, i_die_size)

            bg_s_goods += (u'\n\n\t\t[TOPIC]: {}.\n\t\t[ANSWERS '
                        u'RELATED TO TOPIC]: {}.'
                        u'\n\t\t[ADDS VALUE TO LIBRARY]: {}.'
                        u'\n\t\t[VALUE TO LIBRARY OR '
                        u'COLLECTOR]: {:,.2f}.\n') \
                .format(s_topic, i_die_roll, s_library_TF,
                        f_next_book_value)

        if int(f_book_split) > 0:
            bg_s_goods += (
                u'\n\t\t[OBJECTS]: {:,.2f} sp in materials to '
                u'care for books and scrolls.'
                u'\n\n'.format(f_book_split))

    return bg_s_goods


def narco_generation(d_line, f_split):

    ng_s_goods = ''
    # TODO: Value medicine in goods_medicine.csv

    with open('data/goods/goods_medicines.csv',
              newline='') as gm_f:
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        if f_goods_value_low > f_split:
            s_goods_temp = (u'[DRUG]: Common hedge cure-alls.'
                            u'they might be worth {} sp to the '
                            u'right person.\n') \
                .format(f_split)

            ng_s_goods += textwrap.fill(s_goods_temp, initial_indent
            ='        ',
                                     subsequent_indent=
                                     '        ')
        else:
            ng_s_goods += (u'\n\t\t[BUYER MAX]: '
                        u'{:,.2f}').format(s_price)

            dr_gb = csv.DictReader(gm_f, delimiter=',',
                                   skipinitialspace=True)

            lf_medicine = []
            if_medicine_ctr = -1

            for line_m in dr_gb:
                if_medicine_ctr += 1
                lf_medicine.append(line_m)

            i_medicine_roll = random.randint(0, if_medicine_ctr)
            s_medicine = lf_medicine[i_medicine_roll]['Type']
            ng_s_goods += u'\n\t\t[DRUG CATEGORY]: {}\n'.format(
                s_medicine)

    return ng_s_goods


def religious_art_generation(d_line, f_split):

    rag_s_goods = ''
    # TODO: Value ranges in goods_religious_artifacts.csv
    with open('data/goods/goods_religious_artifacts.csv',
              newline='') as gr_f:
        f_goods_value_low = float(d_line['FLOAT: Value Low'])
        f_goods_value_high = float(d_line['FLOAT: Value High'])
        s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)

        # Case: incoming value lower than bottom of item:
        if f_goods_value_low > f_split:
            s_goods_temp = (
                u'[OBJECT]: A random assortment of goods'
                u'that might be worth {:,.2f} to a prelate '
                u'who is either desperate or taking pity '
                u'on you. ').format(f_split)

            rag_s_goods += '\n'+ textwrap.fill(s_goods_temp, initial_indent=
            '        ',
                                           subsequent_indent=
                                           '        ') + '\n'

        # Case: rolled random price is higher than incoming value
        else:

            dr_gr = csv.DictReader(gr_f, delimiter=',',
                                   skipinitialspace=True)

            lf_religious = []
            if_religious_ctr = -1

            for line_gr in dr_gr:
                if_religious_ctr += 1
                lf_religious.append(line_gr)

            i_religious_roll = random.randint(0, if_religious_ctr)

            s_religious = lf_religious[i_religious_roll]['Object']

            rag_s_goods += u'\n\t\t[OBJECT]: {}'.format(s_religious)
            rag_s_goods += (u'\n\t\t[PRICE TO COLLECTOR '
                        u'OR PRELATE]: {:,.2f}.\n'.format(s_price))

    return rag_s_goods


def wood_generation(d_line, f_split):

    wg_s_goods = ''
    dr_gl = config.csv_to_list('data/goods/goods_woods.csv')
    # TODO: Value lumber in goods_woods.csv

    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    s_price = random.uniform(f_goods_value_low,
                                 f_goods_value_high)





    # Case: incoming value < than lowest item price per unit:
    # Then we make a fake magical wood.
    if f_goods_value_low > f_split:
        lf_wood = []
        if_wood_ctr = -1

        for line_gl in dr_gl:
            if line_gl['Wood Type'] == 'Magical':
                if_wood_ctr += 1
                lf_wood.append(line_gl)

        i_wood_roll = random.randint(0, if_wood_ctr)

        s_wood_type = lf_wood[i_wood_roll]['Name']

        s_goods_temp = (
                u'[WOOD]: A decent fake of {}, looking '
                u'like it might be worth {:,.2f} sp!  '
                u'But only a dishonest carpenter or carver would pay '
                u'anything for it and only then, just a measly '
                u'{:,.2f} sp or so.\n').format(s_wood_type,
                                                s_price,
                                                f_split)
        wg_s_goods += u'\n\t' + textwrap.fill(s_goods_temp, initial_indent=
            '        ',
            subsequent_indent=
            '        ')

    else:
        wg_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                        u'{:,.2f}.\n').format(s_price)
        dr_gl = config.csv_to_list('data/goods/goods_woods.csv')

        i_wood_roll = random.randint(0, len(dr_gl)-1)

        s_wood_type = dr_gl[i_wood_roll]['Name']
        s_wood_desc = u'[PROPERTIES]: '
        s_wood_desc += dr_gl[i_wood_roll]['Description']

        wrap_wood_desc = textwrap.fill(s_wood_desc,
                                           initial_indent=
                                           '        ',
                                           subsequent_indent=
                                           '        ')

        wg_s_goods += (u'\n\t\t[WOOD or IVORY TYPE]: {}\n{}'
                        u'\n').format(s_wood_type, wrap_wood_desc)

    return wg_s_goods


def fabrics_generation(d_line, f_split):

    fg_s_goods = ''
    # TODO: Wait until clothing is done.
    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    s_price = random.uniform(f_goods_value_low, f_goods_value_high)

    fg_s_goods += u'\n\t\t[VALUE PER UNIT]: {:,.2f}.\n'.format(s_price)

    return fg_s_goods


def lab_item_generation(d_line, f_split):

    lig_s_goods = ''
    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    s_price = random.uniform(f_goods_value_low, f_goods_value_high)

    if f_goods_value_low > f_split:
        s_goods_temp = (u'[OBJECT]: A random assortment '
                        u'of junk that might get you '
                        u'{:,.2f} sp.  If you sold it to a '
                        u'Charmed alchemist.'
                        u'\n').format(f_split)

        lig_s_goods += textwrap.fill(s_goods_temp, initial_indent=
        '        ',
                                 subsequent_indent='        ')
    else:
        lig_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                    u'{:,.2f}.\n').format(s_price)

    return lig_s_goods


def utensil_item_generation():

    # Grab some utensils
    l_utensils, i_utensils_ctr = populate_utensils()
    i_utnsl_roll = random.randint(1, len(l_utensils)) - 1

    s_utensil = u'\n\t\t[OBJECT]: {}'.format(l_utensils[i_utnsl_roll]['Object'])

    return s_utensil


def handle_the_rest(d_line, f_split):

    htr_s_goods = ''
    f_goods_value_low = float(d_line['FLOAT: Value Low'])
    f_goods_value_high = float(d_line['FLOAT: Value High'])
    s_price = random.uniform(f_goods_value_low, f_goods_value_high)

    htr_s_goods += (u'\n\t\t[VALUE PER UNIT]: '
                u'{:,.2f}.\n').format(s_price)

    return htr_s_goods


def stone_generation():

    sg_goods = ''
    l_stone = config.csv_to_list('data/goods/goods_stone.csv')
    i_random_stone = random.randint(0, len(l_stone)-1)

    stone_name = (u'\n\t\t[STOME]: ' + l_stone[i_random_stone]['Name'])
    stone_desc = (u'' + l_stone[i_random_stone]['Description'])
    stone_type = (l_stone[i_random_stone]['Type'])

    st_desc_wrap = textwrap.fill(stone_desc,
                                 initial_indent='        ',
                                 subsequent_indent='        ')

    sg_goods += (u'{}. {}.\n{}'.format(stone_name, stone_type, st_desc_wrap))

    return sg_goods

def skins_generation():

    skg_goods = AF.armor_weirdness_animals()

    return skg_goods

