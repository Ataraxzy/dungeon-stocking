#!/usr/bin/env python
""" Basic functions for room stocking.
The meat of the process goes on here, calling the other *funtions.py
files to get to work and return a value.
"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"
# TODO: Refactor using the following website:
# http://chimera.labs.oreilly.com/books/1230000000393/ch01.html#_problem_17
# TODO: Add in GUI components.
# TODO: Randomize Art.[COLLECTOR'S VALUE] so that it != simple hundreds.
# TODO: SP -> GP conversions?  Maybe.  Maybe just print the tables at top.

import random
import csv

import config
import ContainerFunctions as ConFunc
import JewelFunctions as JewelFunc
import ArtFunctions as ArtFunc
import GoodsFunctions as GoodsFunc
import GemFunctions as GemFunc
import CoinFunctions as CoinFunc
import FurnishClothingFunctions as FurnFunc
import MagicItemsFunctions as MagiFunc


#=============================================#
# Main Functions

def empty():
    """Does nothing."""
    # TODO: Tables from Tricks
    return '\nThis room is empty.'


def treasure(f_init_treasure_value):
    """ returns s_main_description, f_new_room_total
    :param f_init_treasure_value: float
    :rtype : str, float
    """

    i_splits = random.randint(1, 7)
    item_number = 0
    f_split_value = f_init_treasure_value / i_splits
    i_trapped_roll = random.randint(1, 20)
    i_hidden_roll = random.randint(1, 20)

    if i_trapped_roll <= 20:
        s_trap_type = ConFunc.container_trapped()

    else:
        s_trap_type = '\n[TRAPPED]: Not Trapped.'

    if i_hidden_roll <= 10:
        s_hidden_container_type = ConFunc.container_hidden()

    else:
        s_hidden_container_type = '\n[HIDDEN]: Not Hidden.'

    s_container_type = ConFunc.container_type()

    s_main_description = (u'\nRoom value, quick estimate: {0:,.2f} sp.\nTreasure is split into: {'
                          u'1} groups.\n==================={2}{3}{4}\n'.format(
        f_init_treasure_value, str(i_splits), s_container_type,
        s_hidden_container_type, s_trap_type))
    f_new_room_total = 0

    while i_splits > 0:
        item_number += 1  # increment counter

        s_value_in_sp, f_adjusted_value = base_value(f_split_value)
        f_new_room_total += f_adjusted_value

        s_main_description += u'\nItem number {0}: [ESTIMATED VALUE]: {1}sp in {2}'.format(
            str(item_number), s_value_in_sp, treasure_type(f_adjusted_value))

        i_splits -= 1  # decrement counter

    return s_main_description, f_new_room_total


def monster():
    """ Does Nothing at the moment """
    # TODO: use CL chart on p.120 in S&W Monster book and then add monsters.
    pass


def trap():
    """ Does Nothing at the moment """
    # TODO: Use trap code from Tricks
    pass


def special():
    """ Does nothing at the moment """
    # TODO: Use the 'special' tables from Tricks
    pass

#=============================================#
# Helper Functions and Tables

def base_value(f_x):
    """ Return sentence delineating the value of a treasure unit
    after pushing it through the randomizer.
    :rtype : str, int
    """

    with open('data/treasuretypes/treasure_base_value.csv', newline='') as \
            bv_f:


        d_bv = csv.DictReader(bv_f, delimiter=',', skipinitialspace=True)
        i_bv = random.randint(1, 100)
        s_sentence = ''

        for line in d_bv:
            adjusted_value = 0
            dieroll = int(line['dieroll'])
            value_modifier = float(line['value_modifier'])

            if i_bv <= dieroll:
                # nonlocal adjusted_value
                adjusted_value = f_x * value_modifier
                s_sentence += u'{:,.2f} '.format(adjusted_value)

                return s_sentence, adjusted_value


def treasure_type(f_adj_treasure_unit):
    """Determine type of treasure: Art, Jewelery &ct.
    :type f_adj_treasure_unit: float
    :rtype : str

    """

    with open('data/treasuretypes/treasure_type.csv', newline='') as tt_f:

        field_names_tt = ['DieRoll', 'Type']
        d_tt = csv.DictReader(tt_f, fieldnames=field_names_tt, delimiter=',',
                              skipinitialspace=True)
        next(d_tt)  # gets rid of the headers

        y_tt = random.randint(1, 20)

        for line in d_tt:
            dieroll = int(line['DieRoll'])
            s_treasure_type = str(line['Type'])

            if y_tt <= dieroll:
                i = s_treasure_type + '.'

                if s_treasure_type == 'Jeweled items':
                    i += JewelFunc.jewelry_type()
                    return i
                elif s_treasure_type == 'Art':
                    i += ArtFunc.art_type(f_adj_treasure_unit)
                    return i
                elif s_treasure_type == 'Goods':
                    i += GoodsFunc.goods_main(f_adj_treasure_unit)
                    return i
                elif s_treasure_type == 'Gems':
                    i += GemFunc.gem_main(f_adj_treasure_unit)
                    return i
                elif s_treasure_type == 'Coins':
                    i += CoinFunc.coin_main(f_adj_treasure_unit)
                    return i
                elif s_treasure_type == 'Furnishings and clothing':
                    i += FurnFunc.furnish_cloth_main(f_adj_treasure_unit)
                    return i
                elif s_treasure_type == 'Special and magic items':
                    i += MagiFunc.magic_item_generator(f_adj_treasure_unit)
                    return i
                else:
                    return i

