#! /usr/bin/env python
""" Functions to populate Coin entries with interesting stuff.

TODO: Meld all the same coins together.
"""

# import statements
import random
import csv
import textwrap

__author__ = 'Roger E. Burgess, III'
__copyright__ = "Copyright 2014, Red Flag & Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or later"
__version__ = "0.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"

def coin_main(f_split_value):
    """

    :param f_split_value: float
    :return: s_coins: str
    """
    # TODO: adding back coinage isn't quite working properly.
    l_coins = []
    s_coins = ''
    # Read in coins.csv:
    with open('data/coins.csv', newline='') as coin_f:
        d_coins = csv.DictReader(coin_f, delimiter=',', skipinitialspace=True)
        for line in d_coins:
            l_coins.append(line)

    # While f_split_value > 0:
    while f_split_value > 10:
        f_sub_split = f_split_value/(random.randint(1, 3))
        f_split_value -= f_sub_split

        # eliminate cruel amounts of copper and brass.
        if f_sub_split > 10000:
            l_coins_listing = high_coins_list(l_coins)

        # eliminate dumb fractions of big coins.
        elif f_sub_split < 400:
            l_coins_listing = low_coins_list(l_coins)

        else:
            l_coins_listing = l_coins

        # randomly select an index value to use
        i_random_coin_index = random.randint(0, len(l_coins_listing)-1)

        # just use the N part, the R part makes things silly with fractional values of whole coins.
        i_whole_part, i_decimal_part = split_floats(f_sub_split)

        # We should, however, put the fractional values BACK into the pool.
        if i_decimal_part > 0:
            f_split_value += i_decimal_part/100

        # This is going to give us fractional coins in some cases.
        f_new_coins_base = i_whole_part * (1/float(l_coins_listing[i_random_coin_index]['Modifier']))
        # so, split'em again.
        i_new_coins_whole, i_new_coins_frac = split_floats(f_new_coins_base)
        #
        # f_split_value += i_new_coins_frac * float(l_coins_listing[i_random_coin_index]['Modifier'])

        i_coin_numb = i_new_coins_whole
        s_coin_type = l_coins_listing[i_random_coin_index]['Coin Type']
        s_coin_name = l_coins_listing[i_random_coin_index]['Name']
        s_coin_desc = (u'[DESCRIPTION]: ' + l_coins_listing[i_random_coin_index]["Description"])
        s_coin_valu = l_coins_listing[i_random_coin_index]['Modifier']
        s_wrap_desc = textwrap.fill(s_coin_desc, initial_indent='        ',
            subsequent_indent='        ')
        f_silver_value = (float(l_coins_listing[i_random_coin_index]['Modifier']) *
        i_coin_numb)
        f_coin_weight = i_coin_numb / 20

        s_coins += ('\n\t[COINS]: {0:,} {1}s ({2}).'
                    '\n\t\t[WORTH]: {3} sp each.'
                    '\n\t\t[VALUE IN SP]: {4:,.2f}'
                    '\n\t\t[WEIGHT]: {5}lbs.'
                    '\n{6}'
                    '\n'.format(i_coin_numb,
                                s_coin_name,
                                s_coin_type,
                                s_coin_valu,
                                f_silver_value,
                                f_coin_weight,
                                s_wrap_desc))

    s_coins_wrap = ('There might be a few copper bits hiding around here, '
                    'good luck finding them.')

    s_coins += '\n' + textwrap.fill(s_coins_wrap, initial_indent='    ',
            subsequent_indent='    ') + '\n'

    return s_coins

def split_floats(f_value):

    i_natural = int(f_value)
    i_decimal = f_value - int(f_value)

    return i_natural, i_decimal


def high_coins_list(l_coin_values):
    l_high_values = []
    for row in l_coin_values:
        if float(row['Modifier']) >= 1:
            l_high_values.append(row)
    return l_high_values


def low_coins_list(l_coin_values):
    l_small_values = []
    for row in l_coin_values:
        if float(row['Modifier']) < 20:
            l_small_values.append(row)
    return l_small_values

def very_low_coins_list(l_coin_values):
    l_tiny_values = []
    for row in l_coin_values:
        if float(row['Modifier']) < 1:
            l_tiny_values.append(row)
    return l_tiny_values
