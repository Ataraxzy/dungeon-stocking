#!/usr/bin/env python
"""


"""

# imports go here, in this order:
# builtins
# third-party
# path changes
# my modules

import random
import csv
import RoomStockingFunctions as RoomStockFunc
import config

__author__ = "Roger E. Burgess, III"
__copyright__ = "Copyright 2014, Red Flag and Roger E. Burgess, III"
__credits__ = ["Courtney Campbell", "Dave Arneson", "Gary Gygax"]
__license__ = "GPL v3 or any later"
__version__ = "1.0.1"
__maintainer__ = "Roger E. Burgess, III"
__email__ = "roger.burgess.iii@gmail.com"
__status__ = "Development"




# How many rooms to stock - get input from user:
number_of_rooms = int(input("How many rooms in your dungeon? "))
#number_of_rooms = 250
inital_xp = int(input("How much XP does your dungeon level need? "))
total_xp, is_int = text_to_int(inital_xp)
if is_int:
    print("{inital_xp} xp.")
else:
    print("{initial_xp} cannot be cast as an int")
    initial_xp = int(input("How much XP does your dungeon level need?"))
#total_xp = 100000
user_era = 3

# TODO: Change User_Era to Tech_Level.
# user_era2 = int(input('''What era are you playing in?
# 1. Ancient days
# 2. Medieval Euro-centric
# 3, Medieval Euro-Centric with other cultural influences
# 4. Modern era
# 5. Sci-fi
# 6. Sword & Sandals'''))

#=============================================#
# Variables and Dictionaries

d_rooms = {}  # create a rooms dictionary.
# index: each_room, value: room_number

d_empty = {}  # empty rooms
# index: e, value: room_number_e
e = 0

d_empty_with_treasure = {}  # empty rooms with treasure
# index: et, value: room_number_et
et = 0

d_monster = {}  # rooms with monsters
# index: m, value: room_number_m
m = 0

d_monster_and_treasure = {}  # rooms with monsters and treasure
# index: mt, value: room_number_mt
mt = 0

d_trap = {}  # trapped rooms
# index: t, value: room_number_t
t = 0

d_trap_and_treasure = {}  # trapped rooms with treasure
# index: tt, value: room_number_tt
tt = 0

d_special = {}  # special rooms
# index: s, value: room_number_s
s = 0

d_treasure_rooms = {}  # listing of rooms with treasure
# index: tr, value: room_number_tr
tr = 0

xp_divisor = 0  # number of rooms with treasure.

#=============================================#
# Basic room code
# Here we generate the basic properties of the
# of each room: Empty|Monster|Treasure|Trap...

# populate with a number of entries from our user;
# set the value to A_Empty as a default.
d_rooms.update({z: 'A_Empty' for z in range(1, number_of_rooms + 1)})

# 1. Fill out the basic contents
#    Empty|Empty+Treasure|Monster|Monster+Treasure|&ct.
# 2. Populate the various dictionaries.
# 3. Calculate the total number of rooms with treasure
# 4. Calculate the treasure value per room with treasure..
for each_room in d_rooms:

    x = random.randint(1, 100)

    if x <= 28:
        d_rooms[each_room] = 'Empty'
        e += 1
        d_empty[e] = each_room

    elif x <= 33:
        d_rooms[each_room] = 'Empty & treasure'
        et += 1
        d_empty_with_treasure[et] = each_room
        xp_divisor += 1
        tr += 1
        d_treasure_rooms[tr] = each_room

    elif x <= 51:
        d_rooms[each_room] = 'Monster'
        m += 1
        d_monster[m] = each_room

    elif x <= 70:
        d_rooms[each_room] = 'Monster & treasure'
        mt += 1
        d_monster_and_treasure[mt] = each_room
        xp_divisor += 1
        tr += 1
        d_treasure_rooms[tr] = each_room

    elif x <= 80:
        d_rooms[each_room] = 'Trap'
        t += 1
        d_trap[t] = each_room

    elif x <= 85:
        d_rooms[each_room] = 'Trap & treasure'
        tt += 1
        d_trap_and_treasure[tt] = each_room
        xp_divisor += 1
        tr += 1
        d_treasure_rooms[tr] = each_room

    elif x <= 100:
        d_rooms[each_room] = 'Special'
        s += 1
        d_special[s] = each_room

    else:
        print("out of bounds, check randint(1, 100)")
if xp_divisor == 0:
    xp_divisor = 1
treasure_unit = round(total_xp / xp_divisor, 2)

with open('Dungeon Contents.csv', 'w+') as initial_contents:
    print('RoomNumber, Contents', file=initial_contents)
    for key, value in d_rooms.items():
        print(u'{0}, {1}'.format(str(key), str(value)), file=initial_contents)
    print("500000000001, fin", file=initial_contents)


# ========================|===============#
# write out treasure type for each room with treasure.

with open('Dungeon Contents.csv', 'r', newline='') as dc_f:
    field_names = ['RoomNumber', 'Contents']  # prep for d_dc
    d_dc = csv.DictReader(dc_f, fieldnames=field_names, delimiter=',',
                          skipinitialspace=True)

    next(d_dc)  # gets rid of the headers

    with open('Dungeon Contents Master.txt', 'w', newline='') as dcm_f:
        total_hoard_value = 0
        tr = ''
        hv = 0.0
        #ph = ''  # Placeholder variable.  base_value() returns str & float,
        #         We only want float.

        for line in d_dc:
            # TODO: Check to see if *container* is valuable.
            room_number = int(line['RoomNumber'])
            room_contents = line['Contents']

            if line['Contents'] == 'Special':
                RoomStockFunc.special()  # Does nothing.
                s_dungeon_words = config.multiple_dungeon_word_choice()
                s_rc = (u'{0}{1}. {2}{3}{4}'.format(config.s_dl_dash,
                                                      str(room_number),
                                                      room_contents,
                                                      s_dungeon_words,
                                                      config.s_dl_dash))
                print(s_rc)
                print(s_rc, file=dcm_f)

            elif line['Contents'] == 'Empty & treasure':
                empty_treasure_unit = treasure_unit / 2
                #ph, empty_treasure_unit = RoomStockFunc.base_value(
                    #empty_treasure_unit)
                s_dungeon_words = config.multiple_dungeon_word_choice()
                tr, hv = RoomStockFunc.treasure(empty_treasure_unit)
                s_rc = (u'{0}{1}. {2}{3}{4}{5}'.format(config.s_dl_dash,
                                                         str(room_number),
                                                         room_contents,
                                                         s_dungeon_words,
                                                         str(tr),
                                                         config.s_dl_dash))
                # Pulled out of s_rc:
                # New Total Room Value: {:,.2f} sp. {} - hv, config.s_dl_dash

                print(s_rc)
                print(s_rc, file=dcm_f)
                total_hoard_value += int(hv * 10 ** 2) / 10 ** 2

            elif line['Contents'] == 'Trap & treasure':
                #ph, treasure_unit = RoomStockFunc.base_value(treasure_unit)
                s_dungeon_words = config.multiple_dungeon_word_choice()
                tr, hv = RoomStockFunc.treasure(treasure_unit)
                s_rc = (u'{0}{1}. {2}{3}{4}{5}'.format(config.s_dl_dash,
                                                         str(room_number),
                                                         room_contents,
                                                         s_dungeon_words,
                                                         str(tr),
                                                         config.s_dl_dash))
                # Pulled out of s_rc:
                # New Total Room Value: {:,.2f} sp. {} - hv, config.s_dl_dash
                print(s_rc)
                print(s_rc, file=dcm_f)
                total_hoard_value += int(hv * 10 ** 2) / 10 ** 2

            elif line['Contents'] == 'Monster & treasure':
                #ph, treasure_unit = RoomStockFunc.base_value(treasure_unit)
                s_dungeon_words = config.multiple_dungeon_word_choice()
                tr, hv = RoomStockFunc.treasure(treasure_unit)
                s_rc = (u'{0}{1}. {2}{3}{4}{5}'.format(config.s_dl_dash,
                                                         str(room_number),
                                                         room_contents,
                                                         s_dungeon_words,
                                                         str(tr),
                                                         config.s_dl_dash))
                print(s_rc)
                print(s_rc, file=dcm_f)
                total_hoard_value += int(hv * 10 ** 2) / 10 ** 2
                # thv = u'\t\tCurrent Dungeon Value: {:,.2f} sp.\n'.format(
                #     total_hoard_value)
                # print(thv)

            # elif room_contents == 'Trap':
            #     RmStFunc.trap()

            # elif room_contents == 'Monster':
            #     RmStFunc.monster()

            elif int(line['RoomNumber']) == 500000000001:
                break

            else:
                empty = RoomStockFunc.empty()
                s_dungeon_words = config.multiple_dungeon_word_choice()
                s_rc = (u'{0}{1}. {2}{3}{4}'.format(config.s_dl_dash,
                                                      str(room_number),
                                                      room_contents,
                                                      s_dungeon_words,
                                                      config.s_dl_dash))
                print(s_rc, file=dcm_f)
                print(s_rc)

with open('Dungeon Contents Master.txt', 'a', newline='') as dcm_f:
    # total_hoard_value = int(total_hoard_value * 10 ** 2) / 10 ** 2
    thv = u'\nTotal Value of All Hoards: {:,.2f} sp.'.format(total_hoard_value)
    print(thv)
    print(thv, file=dcm_f)

# Write the contents of the lists to Dungeon Tables.csv
with open('Dungeon Tables.txt', 'w+', newline='') as dt_f:
    print("\n=============EMPTY ROOMS=================\n", file=dt_f)
    for key, value in d_empty.items():
        print(key, value, file=dt_f)
    #
    print("\n=============EMPTY TREASURE ROOMS========\n", file=dt_f)
    for key, value in d_empty_with_treasure.items():
        print(key, value, file=dt_f)
    #
    print("\n=============MONSTER ROOMS===============\n", file=dt_f)
    for key, value in d_monster.items():
        print(key, value, file=dt_f)
    #
    print("\n=============MONSTER TREASURE ROOMS======\n", file=dt_f)
    for key, value in d_monster_and_treasure.items():
        print(key, value, file=dt_f)
    #
    print("\n=============TRAP ROOMS==================\n", file=dt_f)
    for key, value in d_trap.items():
        print(key, value, file=dt_f)
    #
    print("\n=============TRAP TREASURE ROOMS=========\n", file=dt_f)
    for key, value in d_trap_and_treasure.items():
        print(key, value, file=dt_f)
    #
    print("\n=============SPECIAL ROOMS===============\n", file=dt_f)
    for key, value in d_special.items():
        print(key, value, file=dt_f)


input()
